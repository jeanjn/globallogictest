# GlobalLogic Test

## Caracteristicas Técnicas

- Banco de datos en memoria (h2)
- Construida en Gradle.
- Pruebas unitarias con MockMvc.
- Persistencia con Hibernate.
- Framework Spring Boot.
- Java 8
- Documentación en Swagger



## Funcionalidad

La aplicación debe ejecutarse con gradle, utilizando el comando:
 gradle bootRun o construyendo un .jar con gradle bootJar

- Puerto: 8080
- Swagger: http://localhost:8080/swagger-ui.html
- Usuario para primer acceso:
 {
  "email": "jeanjn24@gmail.com",
  "password": "Abc123"
}




