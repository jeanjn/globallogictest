package com.personal.application.controller;

import java.util.List;
import java.util.Map;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.personal.application.dto.User;
import com.personal.application.exception.EmailException;
import com.personal.application.exception.InvalidPasswordException;
import com.personal.application.exception.ResourceNotFoundException;
import com.personal.application.pojo.Credentials;
import com.personal.application.pojo.LoginResponse;
import com.personal.application.pojo.UserPojo;
import com.personal.application.service.UserService;

@RestController
@RequestMapping("/api/v1")
public class UserController {

	@Autowired
	private UserService userService;

	@PostMapping("/login")
	public LoginResponse login(@RequestBody Credentials credentials)
			throws ResourceNotFoundException, InvalidPasswordException {
		return userService.login(credentials.getEmail(), credentials.getPassword());

	}

	@GetMapping("/user")
	public List<User> getAlluser() {
		return userService.findAll();
	}

	@GetMapping("/user/{id}")
	public ResponseEntity<User> getUserById(@PathVariable(value = "id") String userId)
			throws ResourceNotFoundException {
		return userService.getUserById(userId);
	}

	@PostMapping("/user")
	public User createUser(@Valid @RequestBody UserPojo user) throws Exception {
		User usernew = new User();
		usernew.setId("1237897389b");
		usernew.setName(user.getName());
		usernew.setEmail(user.getEmail());
		usernew.setPassword(user.getPassword());
		usernew.setPhones(user.getPhones());
		return userService.createUser(usernew);
	}

	@PutMapping("/user/{id}")
	public ResponseEntity<User> updateUser(@PathVariable(value = "id") String userId,
			@Valid @RequestBody User userDetails)
			throws ResourceNotFoundException, EmailException, InvalidPasswordException {
		return userService.updateUser(userId, userDetails);
	}

	@DeleteMapping("/user/{id}")
	public Map<String, Boolean> deleteUser(@PathVariable(value = "id") String userId) throws ResourceNotFoundException {
		return userService.deleteUser(userId);
	}

}
