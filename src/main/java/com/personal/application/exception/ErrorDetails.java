package com.personal.application.exception;

public class ErrorDetails {
	private String mensaje;

	public ErrorDetails(String mensaje) {
		this.mensaje = mensaje;
	}

	public String getMensaje() {
		return mensaje;
	}

}