package com.personal.application.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.stream.Collectors;

import org.apache.commons.validator.routines.EmailValidator;
import org.passay.DigitCharacterRule;
import org.passay.LengthRule;
import org.passay.LowercaseCharacterRule;
import org.passay.PasswordData;
import org.passay.PasswordValidator;
import org.passay.Rule;
import org.passay.RuleResult;
import org.passay.UppercaseCharacterRule;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.personal.application.dto.User;
import com.personal.application.exception.EmailException;
import com.personal.application.exception.InvalidPasswordException;
import com.personal.application.exception.ResourceNotFoundException;
import com.personal.application.pojo.LoginResponse;
import com.personal.application.repository.UserRepository;
import com.personal.application.service.UserService;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

@Service
public class UserServiceImpl implements UserService {

	@Autowired
	private UserRepository userRepository;

	@Autowired
	@Qualifier("bcryptPasswordEncoder")
	PasswordEncoder bCryptPasswordEncoder;

	@Bean
	public PasswordEncoder bcryptPasswordEncoder() {
		return new BCryptPasswordEncoder();
	}

	@Override
	public List<User> findAll() {
		return userRepository.findAll();
	}

	@Override
	public ResponseEntity<User> getUserById(String userId) throws ResourceNotFoundException {
		User user = userRepository.findById(userId)
				.orElseThrow(() -> new ResourceNotFoundException("User not found for this id: " + userId));
		return ResponseEntity.ok().body(user);
	}

	@Override
	public User createUser(User user) throws EmailException, InvalidPasswordException {

		boolean valid = EmailValidator.getInstance().isValid(user.getEmail());

		if (!valid) {
			throw new EmailException("Email Inv�lido");
		}

		List<Rule> rules = new ArrayList<>();
		rules.add(new LengthRule(4, 32));
		rules.add(new DigitCharacterRule(2));
		rules.add(new UppercaseCharacterRule(1));
		rules.add(new LowercaseCharacterRule(1));
		PasswordValidator validator = new PasswordValidator(rules);
		PasswordData password = new PasswordData(user.getPassword());
		RuleResult result = validator.validate(password);
		if (!result.isValid()) {
			throw new InvalidPasswordException(
					"La contrase�a debe tener una Mayuscula, letras min�sculas, y dos n�meros.");
		}

		user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
		user.setId(UUID.randomUUID().toString());
		user.setCreated(new Date());
		user.setModified(new Date());
		user.setLast_login(new Date());
		user.setActive(true);
		user.setToken(getJWTToken(user.getName()));

		user.getPhones().stream().forEach(p -> p.setId(UUID.randomUUID().toString()));
		return userRepository.save(user);
	}

	@Override
	public ResponseEntity<User> updateUser(String userId, User userDetails)
			throws ResourceNotFoundException, EmailException, InvalidPasswordException {
		User user = userRepository.findById(userId)
				.orElseThrow(() -> new ResourceNotFoundException("User not found for this id: " + userId));

		if (userDetails.isActive()) {
			user.setActive(true);
		} else {
			user.setActive(false);
		}

		boolean valid = EmailValidator.getInstance().isValid(user.getEmail());

		if (!valid) {
			throw new EmailException("Email Inv�lido");
		}

		List<Rule> rules = new ArrayList<>();
		rules.add(new LengthRule(4, 32));
		rules.add(new DigitCharacterRule(2));
		rules.add(new UppercaseCharacterRule(1));
		rules.add(new LowercaseCharacterRule(1));
		PasswordValidator validator = new PasswordValidator(rules);
		PasswordData password = new PasswordData(user.getPassword());
		RuleResult result = validator.validate(password);
		
		if (!result.isValid()) {
			throw new InvalidPasswordException(
					"La contrase�a debe tener una Mayuscula, letras min�sculas, y dos n�meros.");
		}
		
		user.setEmail(userDetails.getEmail());
		user.setPassword(bCryptPasswordEncoder.encode(userDetails.getPassword()));
		user.setModified(new Date());
		final User updatedUser = userRepository.save(user);
		return ResponseEntity.ok(updatedUser);
	}

	@Override
	public Map<String, Boolean> deleteUser(String userId) throws ResourceNotFoundException {
		User user = userRepository.findById(userId)
				.orElseThrow(() -> new ResourceNotFoundException("User not found for this id: " + userId));

		userRepository.delete(user);
		Map<String, Boolean> response = new HashMap<>();
		response.put("deleted", Boolean.TRUE);
		return response;

	}

	private String getJWTToken(String username) {
		String secretKey = "mySecretKey";
		List<GrantedAuthority> grantedAuthorities = AuthorityUtils.commaSeparatedStringToAuthorityList("ROLE_USER");

		String token = Jwts.builder().setId("JWT").setSubject(username)
				.claim("authorities",
						grantedAuthorities.stream().map(GrantedAuthority::getAuthority).collect(Collectors.toList()))
				.setIssuedAt(new Date(System.currentTimeMillis()))
				.setExpiration(new Date(System.currentTimeMillis() + 7200000))
				.signWith(SignatureAlgorithm.HS512, secretKey.getBytes()).compact();

		return token;
	}

	public LoginResponse login(String email, String password)
			throws ResourceNotFoundException, InvalidPasswordException {

		User userload = userRepository.findByEmail(email)
				.orElseThrow(() -> new ResourceNotFoundException("User not found for this email: " + email));

		if (bCryptPasswordEncoder.matches(password, userload.getPassword()) == false) {
			throw new InvalidPasswordException("Invalid Password");
		}
		;

		String token = getJWTToken(email);
		LoginResponse user = new LoginResponse();
		user.setUser_id(userload.getId());
		user.setLast_login(userload.getLast_login());
		user.setToken(token);
		userload.setLast_login(new Date());
		userRepository.save(userload);
		return user;

	}

}
