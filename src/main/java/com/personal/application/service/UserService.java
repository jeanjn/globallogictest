package com.personal.application.service;

import java.util.List;
import java.util.Map;

import org.springframework.http.ResponseEntity;
import com.personal.application.dto.User;
import com.personal.application.exception.EmailException;
import com.personal.application.exception.InvalidPasswordException;
import com.personal.application.exception.ResourceNotFoundException;
import com.personal.application.pojo.LoginResponse;


public interface UserService {
	public List<User> findAll();
	public ResponseEntity <User> getUserById(String userId) throws ResourceNotFoundException;
	public User createUser(User user) throws EmailException, InvalidPasswordException;
	public ResponseEntity <User> updateUser(String userId, User userDetails) throws ResourceNotFoundException, EmailException, InvalidPasswordException;
	public Map <String, Boolean> deleteUser(String userId) throws ResourceNotFoundException;
	public LoginResponse login(String email, String password) throws ResourceNotFoundException, InvalidPasswordException;
	

}
