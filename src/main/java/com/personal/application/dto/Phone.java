package com.personal.application.dto;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

import io.swagger.annotations.ApiModelProperty;

@Entity
@Table(name = "phone")
public class Phone {
	@Id
	@ApiModelProperty(hidden = true, access="hidden") 
	@JsonIgnore 
	private String id;
	private Integer number;
	private Integer citycode;
	private Integer countrycode;
	
	@ApiModelProperty(hidden = true) 
	@JsonIgnore 
	private String userId;
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "userId", nullable = false, insertable = false, updatable = false)
	@ApiModelProperty(hidden = true)
	@JsonIgnore
	private User user;

	public Phone() {

	}

	public Phone(String id, Integer number, Integer citycode, Integer countrycode, String userId) {
		this.id = id;
		this.number = number;
		this.citycode = citycode;
		this.countrycode = countrycode;
		this.userId = userId;
	}



	public void setId(String id) {
		this.id = id;
	}

	public Integer getNumber() {
		return number;
	}

	public void setNumber(Integer number) {
		this.number = number;
	}

	public Integer getCitycode() {
		return citycode;
	}

	public void setCitycode(Integer citycode) {
		this.citycode = citycode;
	}

	public Integer getCountrycode() {
		return countrycode;
	}

	public void setCountrycode(Integer countrycode) {
		this.countrycode = countrycode;
	}



	public void setUserId(String userId) {
		this.userId = userId;
	}

}
