package com.personal.application.controller;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.personal.application.dto.Phone;
import com.personal.application.dto.User;
import com.personal.application.pojo.Credentials;
import com.personal.application.repository.UserRepository;
import com.personal.application.service.UserService;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@AutoConfigureMockMvc
class UserControllerTest {

	@Autowired
	private MockMvc mockMvc;

	@Autowired
	private ObjectMapper objectMapper;

	@Autowired
	private UserRepository userRepository;

	@Mock
	private UserService userService;

	@InjectMocks
	private UserController userController;


	@Test
	void createUserTest() throws Exception {

		User user = new User();
		user.setName("Jean");
		user.setEmail("jeanjn244555@gmail.com");
		user.setPassword("Abc123323");

		Set<Phone> phones = new HashSet<Phone>();

		Phone ph = new Phone();
		ph.setCountrycode(56);
		ph.setCitycode(9);
		ph.setNumber(32804207);
		phones.add(ph);

		user.setPhones(phones);

		mockMvc.perform(
				post("/api/v1/user").contentType("application/json").content(objectMapper.writeValueAsString(user)))
				.andExpect(status().isOk());

		Optional<User> userEntity = userRepository.findByEmail("jeanjn244555@gmail.com");
		assertThat(userEntity.get().getName()).isEqualTo("Jean");
	}

	@Test
	void getAllUsersTest() throws Exception {

		mockMvc.perform(get("/api/v1/user")).andDo(print()).andExpect(status().isOk());

		List<User> users = userRepository.findAll();
		assertThat(users.get(0).getEmail()).isEqualTo("jeanjn24@gmail.com");
	}

	@Test
	void getUserByIdTest() throws Exception {
		Optional<User> user = userRepository.findById("1b2668e7-0d5a-4bb8-b089-95bc2280ce93");
		mockMvc.perform(get("/api/v1/user/{id}", "1b2668e7-0d5a-4bb8-b089-95bc2280ce93")).andExpect(status().isOk());
		assertThat(user.get().getId()).isEqualTo("1b2668e7-0d5a-4bb8-b089-95bc2280ce93");
		assertThat(user.get().getEmail()).isEqualTo("jeanjn24@gmail.com");
	}
	
	@Test
	void globalExceptionTest() throws Exception {
		userRepository.findById("1b2668e7-0d5a-4bb8-b089-95bc2280ce9377");
		mockMvc.perform(get("/api/v1/user/{id}", "1b2668e7-0d5a-4bb8-b089-95bc2280ce9377")).andExpect(status().isNotFound());
	}


	@Test
	void deteleUserById() throws Exception {
		User user = new User();
		user.setId("1b2668e7-0d5a-4bb8-b389-95bc2280ce93");
		user.setName("Jean");
		user.setEmail("jeanjn24455@gmail.com");
		user.setPassword("Abc123323");
		Set<Phone> phones = new HashSet<Phone>();
		Phone ph = new Phone();
		ph.setId("8b2668e7-0d5a-4bb8-b389-95bc2280ce93");
		ph.setCountrycode(56);
		ph.setCitycode(9);
		ph.setNumber(32804207);
		phones.add(ph);
		user.setPhones(phones);
		userRepository.save(user);
		mockMvc.perform(MockMvcRequestBuilders.delete("/api/v1/user/{id}", "1b2668e7-0d5a-4bb8-b389-95bc2280ce93"))
				.andExpect(status().isOk());

	}

	@Test
	void updateUserTest() throws Exception {

		User user = new User();
		user.setId("1b2668e7-0d5a-4bb8-b389-95bc2280ce93");
		user.setName("Jean");
		user.setEmail("jeanjn24466@gmail.com");
		user.setPassword("Abc123323");
		Set<Phone> phones = new HashSet<Phone>();
		Phone ph = new Phone();
		ph.setId("8b2668e7-0d5a-4bb8-b389-95bc2280ce93");
		ph.setCountrycode(56);
		ph.setCitycode(9);
		ph.setNumber(32804207);
		phones.add(ph);
		user.setPhones(phones);
		userRepository.save(user);

		user.setActive(false);
		User usr = userRepository.save(user);

		mockMvc.perform(put("/api/v1/user/{id}", "1b2668e7-0d5a-4bb8-b389-95bc2280ce93").contentType("application/json")
				.content(objectMapper.writeValueAsString(user))).andExpect(status().isOk());

		assertThat(!usr.isActive());
	}
	
	@Test
	void LoginTest() throws Exception {
		
		Credentials cred = new Credentials();
		cred.setEmail("jeanjn24@gmail.com");
		cred.setPassword("Abc123");
		mockMvc.perform(
				post("/api/v1/login").contentType("application/json").content(objectMapper.writeValueAsString(cred)))
				.andExpect(status().isOk());

		Optional<User> userEntity = userRepository.findByEmail("jeanjn24@gmail.com");
		assertThat(userEntity.get().getName()).isEqualTo("Howard");
	}
	
	@Test
	void passwordExceptionTest() throws Exception {
		Credentials cred = new Credentials();
		cred.setEmail("jeanjn24@gmail.com");
		cred.setPassword("Abc1234");
		mockMvc.perform(
				post("/api/v1/login").contentType("application/json").content(objectMapper.writeValueAsString(cred)))
				.andExpect(status().isBadRequest());

	}

}