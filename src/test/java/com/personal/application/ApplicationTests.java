package com.personal.application;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.personal.application.dto.Phone;
import com.personal.application.dto.User;
import com.personal.application.pojo.PhonePojo;
import com.personal.application.repository.UserRepository;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@AutoConfigureMockMvc
class ApplicationTests {

	@Autowired
	private MockMvc mockMvc;

	@Autowired
	private ObjectMapper objectMapper;

	@Autowired
	private UserRepository userRepository;

	@Test
	void registrationTest() throws Exception {

		User user = new User();
		user.setName("Jean");
		user.setEmail("jeanjn244@gmail.com");
		user.setPassword("Abc123323");

		Set<Phone> phones = new HashSet<Phone>();

		Phone ph = new Phone();
		ph.setCountrycode(56);
		ph.setCitycode(9);
		ph.setNumber(32804207);
		phones.add(ph);

		user.setPhones(phones);
		
		PhonePojo pj = new PhonePojo();
		pj.setCitycode(123);
		pj.setCountrycode(56);
		pj.setNumber(76456735);
		
		

//		Set<Phone> phones = new HashSet<Phone>();
//		phones.add(new Phone("1b9668e7-0d5a-4bb8-b089-95bc2280ce96", 9, 56, 32804207,
//				"1b3668e7-0d5a-4bb8-b089-95bc2280ce93"));
//		User user = new User("1b3668e7-0d5a-4bb8-b089-95bc2280ce93", "Howard", "jeanjn24@gmail.com", "Abc123",
//				new Date(), new Date(), new Date(),
//				"eyJhbGciOiJIUzUxMiJ9.eyJqdGkiOiJKV1QiLCJzdWIiOiJBYjEyIiwiYXV0aG9yaXRpZXMiOlsiUk9MRV9VU0VSIl0sImlhdCI6MTU4MDc0NzUyMCwiZXhwIjoxNTgwNzU0NzIwfQ.IHi7U0QF8NO4LZC1VDwAv01tkRstThx6nZerCkcGGVOFDDAbHDcMlTNiw3mYbcHzzJUH8SFtJKgqSFtKfqHsZQ",
//				true, phones);
		

		mockMvc.perform(
				post("/api/v1/user").contentType("application/json").content(objectMapper.writeValueAsString(user)))
				.andExpect(status().isOk());

		Optional<User> userEntity = userRepository.findByEmail("jeanjn244@gmail.com");
		assertThat(userEntity.get().getEmail()).isEqualTo("jeanjn244@gmail.com");
		assertThat(pj.getCitycode()).isEqualTo(123);
	}

}
